# OH WHAT'S THIS THE PROJECT HAS MOVED

It's now over at [Github](https://github.com/Iojioji/Taiko-SV-Viewer)



A tiny program that allows you to view an osu map's SV as a graph

You can check out other mode's maps. I haven't really tested those but it should work



### TODOS
- Settings
    - Change graph colors
    - Change marker style (color, size, show/hide)
- Open multiple diffs
- Add drag-and-drop to open diffs
- Change marker size depending on zoom level
- Add auto-update thing
